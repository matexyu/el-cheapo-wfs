#!/bin/bash

echo Stopping the two 'audioadapter' clients Device0 and Device1 ...
jack_unload Device0
jack_unload Device1
killall jackd
killall qjackctl
echo ...done
