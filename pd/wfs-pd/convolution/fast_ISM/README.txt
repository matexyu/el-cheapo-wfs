
=== fast_ISM_RoomResp.m	===

Computes the RIR between a source and a receiver in a given simulation setup, using the fast image-source method described in [1]. This implementation generates the early reflections using the standard ISM algorithm described in [2], while the late reverberation phase is computed as random noise decaying according to the predicted energy-decay curve for the given environment. An example RIR obtained with this implementation can be found further below on this page.


=== fast_ISM_RIR_bank.m	===

Computes a bank of RIRs (corresponding to a simulation setup defined in a file such as ISM_setup.m) according to the fast ISM method implemented in fast_ISM_RoomResp.m. One impulse response is computed for every possible combination of the sensors and source trajectory points.


=== ISM_vs_FastISM_demo.m ===

Demonstration file displaying some typical RIR results from the fast ISM implementation (fast_ISM_RoomResp.m) as well as the same results obtained using the standard ISM simulation (ISM_RoomResp.m). This function randomly selects a room setup and plots each RIR example (and its EDC) in a figure for comparison purposes.


=== logistic_rnd.m ===

Generates random samples distributed according to the logistic PDF.
