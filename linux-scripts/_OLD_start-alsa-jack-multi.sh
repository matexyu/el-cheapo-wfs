#!/bin/bash

# Script to have the two 6 channels soundcards appearing into qjackctl via 'alsa_out'

# NOTES: 
# - add  2>&1 1> /dev/null at the end of alsa_out parameters to drop stderr console outputs

# Jack - "Cannot lock down memory area (Cannot allocate memory)"
#  sudo dpkg-reconfigure -p high jackd
#  sudo adduser <username> audio
#
# Source: http://ubuntuforums.org/showthread.php?t=1637399

set -e

sleep  1

echo Starting Device0...
alsa_out -d hw:Device -c 8 -j Device0 -q 0 -r 44100 &
echo ...done

sleep  1

echo Starting Device1...
alsa_out -d hw:Device_1 -c 8 -j Device1 -q 0 -r 44100 &
echo ...done

