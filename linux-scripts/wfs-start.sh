#!/bin/bash
reset

set -e

echo "---------------------------------------"
echo "               WFS START :)"
echo "---------------------------------------"

# NOTE: if suddenly Jack fails initializing without apparent reson with the error:
#
#   "D-BUS: JACK server could not be started. 
#    Sorry Cannot connect to server socket err = No such file or directory"
#
# it could be the result of Jack selecting a bad audio device.
# in this case, using qjackctl: in the setup menu, look for the "Interface" option, 
# and don't hit the dropdown menu, but hit the arrow ">" next to it.
# This will list devices reported by alsa.
# Try the devices on that list till one of them works. "hw:Generic" (or you integrated soundcard) 
# is likely what you're looking for.

echo "Starting Jack server / qjackctl..."

qjackctl &

sleep 2

echo "...done"

echo ""
echo "- - - - - - - - - - - - - - - - - - - -"
echo " Starting multiple output devices..."
echo "- - - - - - - - - - - - - - - - - - - -"
# Two 8 channels soundcards appearing into qjackctl via 'audioadapter' (Jack2 / jackmp)
# NOTES: 
# - add  2>&1 1> /dev/null at the end of alsa_out parameters to drop stderr console outputs
# Jack - "Cannot lock down memory area (Cannot allocate memory)"
#  sudo dpkg-reconfigure -p high jackd
#  sudo adduser <username> audio
# Source: http://ubuntuforums.org/showthread.php?t=1637399
# audioadapter use: http://ubuntuforums.org/archive/index.php/t-1128748.html

echo Loading Device0 ...

jack_load -i"-q0 -r44100 -d hw:Device -i2 -o8" Device0 audioadapter &

echo ""
echo ...done

echo "- - - - - - - - - - - - - - - - - - - -"
echo Loading Device1...

jack_load -i"-q0 -r44100 -d hw:Device_1 -i2 -o8" Device1 audioadapter & 

echo ""
echo ...done
echo "- - - - - - - - - - - - - - - - - - - -"
