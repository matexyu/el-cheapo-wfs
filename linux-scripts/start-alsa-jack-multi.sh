#!/bin/bash

# Script to have the two 8 channels soundcards appearing into qjackctl via 'audioadapter' (Jack2 / jackmp)

# NOTES: 
# - add  2>&1 1> /dev/null at the end of alsa_out parameters to drop stderr console outputs

# Jack - "Cannot lock down memory area (Cannot allocate memory)"
#  sudo dpkg-reconfigure -p high jackd
#  sudo adduser <username> audio
# Source: http://ubuntuforums.org/showthread.php?t=1637399

# audioadapter use: http://ubuntuforums.org/archive/index.php/t-1128748.html

set -e

echo ""
echo "- - - - - - - - - - - - - - - - - - - -"
echo Loading Device0 ...
jack_load -i"-d hw:Device -i2 -o8" Device0 audioadapter 
echo ""
echo ...done
echo "- - - - - - - - - - - - - - - - - - - -"
echo ""
echo "- - - - - - - - - - - - - - - - - - - -"
echo Loading Device1...
jack_load -i"-d hw:Device_1 -i2 -o8" Device1 audioadapter 
echo ""
echo ...done
echo "- - - - - - - - - - - - - - - - - - - -"
echo ""

